﻿using System;
using System.Threading.Tasks;

namespace Riego
{
	public static class ConnectionManager
	{
		public static async Task<Tuple<Error, string>> Post(string endpoint, string parameters) 
		{
			Error error = null;
			string content = string.Empty;
			// se envia la post al servidor con su endpoint y sus parametros
			var res= await RestService.PostToServer(endpoint, parameters);
			// se verifica el status code del response
			error= HttpAnalysis.VerifyHttpStatus(res);
			if (error == null)
				content = await res.Content.ReadAsStringAsync();
			
			// se retorna true solo cuando el status code es 200 y el contenido del response
			return new Tuple<Error,string>(error, content);
		}
		public static async Task<Tuple<Error, string>> Get(string endpoint, string parameters) 
		{
			Error error = null;
			string content = string.Empty;
			// se envia la get al servidor con su endpoint y sus parametros
			var res = await RestService.GetToServer(endpoint, parameters);
			// se verifica el status code del response
			error = HttpAnalysis.VerifyHttpStatus(res);
			if (error == null)
				content = await res.Content.ReadAsStringAsync();

			// se retorna true solo cuando el status code es 200 y el contenido del response
			return new Tuple<Error, string>(error, content);
		}
			

	}
}
