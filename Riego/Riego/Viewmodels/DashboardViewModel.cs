﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Riego
{
	public class DashboardViewModel: INotifyPropertyChanged
	{
		public DashboardViewModel(ListRaspberry l)
		{
			ListRaspberrys = l;
			raspberrySelected = ListRaspberrys.First();
		}
		ListRaspberry listRaspberrys = null;
		public ListRaspberry ListRaspberrys 
		{
			get { return listRaspberrys;}
			set { listRaspberrys = value; OnPropertyChanged("ListRaspberrys");}
		}
		Raspberry raspberrySelected = null;
		public Raspberry RaspberrySelected
		{
			get { return raspberrySelected;}
			set { raspberrySelected = value; OnPropertyChanged("RaspberrySelected");}
		}
		Raspberry raspberrySelectedAux = null;
		public Raspberry RaspberrySelectedAux
		{
			get { return raspberrySelectedAux; }
			set { raspberrySelectedAux = value; OnPropertyChanged("RaspberrySelectedAux"); }
		}

		ICommand showAllRaspberrys = null;
		public ICommand ShowAllRaspberrys 
		{
			get 
			{
				if (showAllRaspberrys == null) 
				{
					showAllRaspberrys = new Command((par) => 
					{
						if (ListRaspberrys.Count == 1)
						{
							// mostrar mensaje diciendo que tiene solo 1

						}
						else 
						{
							// asignar auxraspberry
							RaspberrySelectedAux = RaspberrySelected;
							
							// mostrar modal con la lista de raspberrys
							App.Navigation.PushModalAsync(new Raspberrys(this));
						}

					}, (par) => { return true; });	
				}
				return showAllRaspberrys; 
			}
		}

		ICommand applyChanged = null;
		public ICommand ApplyChanged
		{
			get 
			{
				if (applyChanged == null) 
				{
					applyChanged = new Command(async (par) =>
					{
						// aceptar el cambio de raspberry
						RaspberrySelected = RaspberrySelectedAux;
						// popView;
						await PopRaspberrysPage();
						//  actualizar datos
						UpdateAllValues();
					},
											   (par) => 
					{
						return true;
					});
				}
				return applyChanged;
			}
		}
		ICommand cancelChanged = null;
		public ICommand CancelChanged
		{
			get
			{
				if (cancelChanged == null)
				{
					cancelChanged = new Command(async(par) =>
					{
						//volver cancelar el cambio de raspberry
						await PopRaspberrysPage();


					},
											   (par) =>
					{
						return true;
					});
				}
				return cancelChanged;
			}
		}
		private async Task<Page> PopRaspberrysPage() 
		{
			return await App.Navigation.PopModalAsync();
		}

		private async Task UpdateAllValues() 
		{
			//Todo actualizar modulo luz

			//Todo actualizar modulo riego
			//Todo actualizar modulo ventilacion
			//Todo actualizar modulo extractor aire
			//Todo actualizar modulo temperatura/humedad ambiente
			//Todo actualizar modulo Foto
			//Todo actualizar modulo temperatura/humedad maceta
			//Todo actualizar modulo ph
			//Todo actualizar modulo notificaciones 


		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this,
					new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
