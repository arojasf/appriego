﻿using System;


namespace Riego
{
	public static class ContentAnalisys
	{
		public static Error VerifyContentAnalisys(string content)
		{
			var error= new Error(content);

			if (error.code == null)
				return null;
			return error;

		}
	}
}
