﻿using System;
using System.Threading.Tasks;

namespace Riego
{
	public interface IUserSession
	{
		Task<bool> LoginToServer(string mail, string password);
		Task<bool> RegisterToServer(string name, string mail, string password);
		Task<bool> AlreadyExistsMail(string mail);

		bool PopulateRegisterToServer(string content);
		bool PopulateAlreadyExistsMail(string content);
		bool Populate(string content);

		Error GetError();
	}
}
