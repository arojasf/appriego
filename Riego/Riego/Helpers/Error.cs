﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Riego
{
	public class Error
	{
		EnumErrors tipe;

		[JsonProperty("code")]
		public string code { get; set; }
		[JsonProperty("desc")]
		public string desc { get; set; }

		public EnumErrors GetTypeError() 
		{
			return tipe;
		}

		public string GetMessage() 
		{
			string msg = string.Empty;
			switch (tipe)
			{
				case EnumErrors.timeOut:
					msg = ErrorMessages.timeout;
					break;
				default:
					msg = ErrorMessages.generic;
					break;
			}
			switch (code) 
			{
				case "10001":
					msg = ErrorMessages.NoExisteUser;
					break;
				default:
					msg = ErrorMessages.generic;
					break;
			}
			return msg;
		}

		public Error()
		{
			tipe= EnumErrors.generico;
		}
		public Error(EnumErrors tipeError) 
		{
			tipe = tipeError;
		}
		public Error(string content) 
		{
			if (string.IsNullOrWhiteSpace(content))
				tipe = EnumErrors.content;
			else 
			{
				JToken token = JObject.Parse(content);

				code = (string)token.SelectToken("error.code");
				 desc= (string)token.SelectToken("error.desc");

				tipe = EnumErrors.negocio;
			}
		}
	}
}
