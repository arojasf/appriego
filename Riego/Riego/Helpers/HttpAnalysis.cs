﻿using System;
using System.Net.Http;

namespace Riego
{
	public static class HttpAnalysis
	{
		public static Error VerifyHttpStatus(HttpResponseMessage httpResponse) 
		{
			Error error = null;
			if (httpResponse == null) 
			{
				return new Error(EnumErrors.timeOut);
			}
				
			
			switch (httpResponse.StatusCode)
			{
				case System.Net.HttpStatusCode.OK:
					break;
				default:
					//todo  cambiar al error del code status 401, 404, 504, etc
					error = new Error(EnumErrors.generico);
					break;
			}

			return error;
		
		}
	}
}
