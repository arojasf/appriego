﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Riego
{
	public partial class Dashboard : ContentPage
	{
		DashboardViewModel vm = null;
		public Dashboard(ListRaspberry l)
		{
			InitializeComponent();
			vm = new DashboardViewModel(l);
			this.BindingContext = vm;
		}


	}
}
