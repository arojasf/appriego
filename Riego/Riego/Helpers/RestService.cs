﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Riego
{
	public class RestService:IRestService
	{
		static RestService restService=null;
		HttpClient _client = null;
		protected HttpClient client
		{
			get { return _client;}
			set { _client = value;}

		}
		public RestService()
		{

			client = new HttpClient();
			client.Timeout = new TimeSpan(0,0,10);
			//client.DefaultRequestHeaders.Add();

		}
		public static RestService Init() 
		{
			if (restService == null)
				restService = new RestService();
			return restService;
		}
		public static async Task<HttpResponseMessage> PostToServer(string endpoint,string content) 
		{
			Uri url = new Uri(Constants.urlServer+endpoint, UriKind.RelativeOrAbsolute);
			HttpResponseMessage res= null;
			HttpContent httpcontent = new StringContent(content,Encoding.UTF8, "application/json");

			try
			{
				res = await restService.client.PostAsync(url, httpcontent, new CancellationToken()).ConfigureAwait(false);

			}
			catch (WebException e)
			{
				
			}
			return res;
		}
		public static async Task<HttpResponseMessage> GetToServer(string endpoint, string parameter)
		{
			Uri url = new Uri(Constants.urlServer + endpoint+parameter, UriKind.RelativeOrAbsolute);
			HttpResponseMessage res = null;
			//HttpContent httpcontent = new StringContent(content, Encoding.UTF8, "application/json");

			try
			{
				res = await restService.client.GetAsync(url).ConfigureAwait(false);

			}
			catch (WebException e)
			{

			}
			return res;
		}

	}
}
