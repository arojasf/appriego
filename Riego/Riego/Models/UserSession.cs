﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Riego
{
	public class UserSession: IUserSession
	{
		public string username { get; set; }
		public string mail { get; set; }
		public string token { get; set; }

		private static UserSession instance;
		public static UserSession Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new UserSession();
				}
				return instance;
			}
		}

	     Error error = null;
		public UserSession()
		{
			
		}
		public void Init()
		{
			
			 
		}
		private  string ParametersForLogin( string mail, string password) 
		{
			dynamic obj = new
			{
				mail =mail,
				password = password,
				app = Constants.app
			};
			dynamic usuario = new
			{
				user = obj
			};
			var json = JsonConvert.SerializeObject(usuario);
			return json;
		}
		private  string ParametersForRegister(string name, string mail, string password)
		{
			dynamic obj = new
			{
				nombre = name,
				mail = mail,
				password = password,
				app = Constants.app
			};
			dynamic usuario = new
			{
				user = obj
			};
			var json = JsonConvert.SerializeObject(usuario);
			return json;
		}

		public  async Task<bool> LoginToServer( string mail, string password ) 
		{
			bool status = false;
			var res=await ConnectionManager.Post(Constants.endPointLogin, ParametersForLogin(mail,password));
			if (res.Item1==null)
			{
				//  res.item2 que treae el contenido
				status = Populate(res.Item2);

			}
			else 
			{
				//traigo el error del connectionManager
				error = res.Item1;
			}
			return status;
		}
		public  async Task<bool> RegisterToServer(string name,string mail, string password )
		{
			bool status = false;
			var res = await ConnectionManager.Post(Constants.endPointSignUp, ParametersForRegister(name, mail,password));
			if (res.Item1 == null)
			{
				//  res.item2 que treae el contenido
				status = PopulateRegisterToServer(res.Item2);

			}
			else
			{
				//traigo el error del connectionManager
				error = res.Item1;
			}
			return status;
		}
		public  async Task<bool> AlreadyExistsMail(string mail) 
		{
			bool status = false;
			string url = string.Format("?mail={0}&app={1}",mail,Constants.app);
			var res = await ConnectionManager.Get(Constants.endPointAlreadyExistsMail, url );
			if (res.Item1 == null)
			{
				//analizamos el contentido para saber si trae un error
				error=ContentAnalisys.VerifyContentAnalisys(res.Item2);
				//  res.item2 que treae el contenido
				if(error==null)
					status = PopulateAlreadyExistsMail(res.Item2);

			}
			else
			{
				//traigo el error del connectionManager
				error = res.Item1;
			}
			return status;
		}
		public  bool PopulateAlreadyExistsMail(string content) 
		{
			bool status = false;
			try
			{
				//Deserializar el contenido y armar el modelo
				JToken token = JObject.Parse(content);

				status = (bool)token.SelectToken("exists");

			}
			catch (Exception e)
			{
				error = new Error(EnumErrors.content);
			}
			return status;
		}
		public  bool PopulateRegisterToServer(string content) 
		{
			bool status = false;
			try
			{
				//Deserializar el contenido y armar el modelo
				var a = JsonConvert.DeserializeObject(content);
				status = true;
			}
			catch (Exception e)
			{
				error = new Error(EnumErrors.content);
			}
			return status;
		
		}
		public  bool Populate(string content) 
		{
			bool status = false;
			try 
			{
				//Deserializar el contenido y armar el modelo
				var a=JsonConvert.DeserializeObject(content);
				status = true;
			}
			catch(Exception e)
			{
				error = new Error(EnumErrors.content);
			}
			return status;
			
		}

		public  Error GetError() 
		{
			return error;
		}
		//public static async Task SignUpToServer()
		//{
		//	await RestService.PostToServer(Constants.endPointSignUp, Parameters());

		//}
		//public static async Task VerifyMailToServer()
		//{
		//	await RestService.PostToServer(Constants.endPointVerifyMail, Parameters());

		//}
	}
}
