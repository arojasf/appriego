﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Riego
{
	public class LoginViewModel: INotifyPropertyChanged
	{
		public LoginViewModel()
		{
		}
		//string _mail = string.Empty;
		string _mail = "Rojas.arturo.05@gmail.com";
		public string mail
		{
			get { return _mail; }
			set { _mail = value; OnPropertyChanged("mail"); }
		}
		//string _password = string.Empty;
		string _password = "123123";
		public string password
		{
			get { return _password; }
			set { _password = value; OnPropertyChanged("password"); }
		}
		string _username = string.Empty;
		public string username
		{
			get { return _username; }
			set { _username = value; OnPropertyChanged("username"); }
		}

		string _msg = string.Empty;
		public string msg 
		{
			get { return _msg;}
			set { _msg = value; OnPropertyChanged("msg"); }
		}

		bool _isVisibleFieldsForSignup = false;
		public bool IsVisibleFieldsForSignup
		{
			get{ return _isVisibleFieldsForSignup; }
			set { _isVisibleFieldsForSignup = value;OnPropertyChanged("IsVisibleFieldsForSignup"); }

		}
		bool _iVisibleFieldsForLogin = true;
		public bool IsVisibleFieldsForLogin
		{
			get { return _iVisibleFieldsForLogin; }
			set { _iVisibleFieldsForLogin = value; OnPropertyChanged("IsVisibleFieldsForLogin"); }

		}
		bool _indicatorLoading = false;
		public bool indicatorLoading 
		{
			get { return _indicatorLoading;}
			set { _indicatorLoading = value; OnPropertyChanged("indicatorLoading");}
		}
		bool _indicatorMsgVisibility = false;
		public bool indicatorMsgVisibility
		{
			get { return _indicatorMsgVisibility; }
			set { _indicatorMsgVisibility = value; OnPropertyChanged("indicatorMsgVisibility"); }
		}
		bool _isVisibleMailEntry = true;
		public bool isVisibleMailEntry
		{
			get { return _isVisibleMailEntry;}
			set { _isVisibleMailEntry = value; OnPropertyChanged("isVisibleMailEntry");}

		}


		ICommand _signupViewCommand = null;
		public ICommand SignupViewCommand 
		{
			get 
			{
				if (_signupViewCommand == null) 
				{
					_signupViewCommand = new Command((par) =>
					{
						IsVisibleFieldsForSignup = true;
						IsVisibleFieldsForLogin = false;


					}, (par) => { return true; });
				}
				return _signupViewCommand;
			}
		}
		ICommand _loginViewCommand = null;
		public ICommand LoginViewCommand
		{
			get
			{
				if (_loginViewCommand == null)
				{
					_loginViewCommand = new Command((par) =>
					{
						
						IsVisibleFieldsForLogin = true;
						IsVisibleFieldsForSignup = false;

					}, (par) => { return true; });
				}
				return _loginViewCommand;
			}
		}


		ICommand _loginCommand = null;
		public ICommand LoginCommand
		{
			get
			{
				if (_loginCommand == null)
				{
					_loginCommand = new Command(async(par) =>
					{
						//validar si tiene internet
						//TODO

						//ValidarConnection();

						//validar para el login
						// mail y password

						//ValidarMail();
						//ValidarPassword();
						indicatorLoading = true;
						//el usuario se logea al server
						var status=await UserSession.Instance.LoginToServer(mail,password);
						//retorna si se pudo logear o no
						if (status)
						{
							var l=await	ListRaspberry.Init();
						
							if (l.Item1)
							{
								if (l.Item2.Count > 0)
									await App.Navigation.PushAsync(new Dashboard(l.Item2));
								else
								{
									msg = ErrorMessages.UserWithoutRaspberrys;
									indicatorMsgVisibility = true;
								}
							}
							else
							{
								msg = UserSession.Instance.GetError().GetMessage();
								indicatorMsgVisibility = true;
							}
						}
						else 
						{
							msg = UserSession.Instance.GetError().GetMessage();
							indicatorMsgVisibility = true;
						}
						indicatorLoading = false;
						


					}, (par) => { return true; });
				}
				return _loginCommand;
			}
		}
		ICommand _signupCommand = null;
		public ICommand SignupCommand
		{
			get
			{
				if (_signupCommand == null)
				{
					_signupCommand = new Command(async(par) =>
					{
						//validar si tiene internet
						//TODO

						//ValidarConnection();

						//validar para el login
						// mail y password

						//ValidarMail();
						//ValidarPassword();
						indicatorLoading = true;
						//el usuario se logea al server
						var status = await UserSession.Instance.RegisterToServer(username,mail, password);
						//retorna si se pudo logear o no
						if (status)
						{
							
							msg = "usuario agregado";
							indicatorMsgVisibility = true;
							IsVisibleFieldsForLogin = true;
							IsVisibleFieldsForSignup = false;
							username = string.Empty;
							password = string.Empty;
							mail = string.Empty;
						}
						else
						{
							msg = UserSession.Instance.GetError().GetMessage();
							indicatorMsgVisibility = true;
						}
						indicatorLoading = false;

					}, (par) => { return true; });
				}
				return _signupCommand;
			}
		}
		ICommand _verifyMailCommand = null;
		public ICommand VerifyMailCommand 
		{
			get 
			{
				if (_verifyMailCommand == null) 
				{
					_verifyMailCommand = new Command(async(par) => 
					{
						//validar si tiene internet
						//TODO

						//ValidarConnection();

						//validar para el login
						// mail y password

						//ValidarMail();
						//ValidarPassword();
						indicatorLoading = true;
						//el usuario se logea al server
						var status = await UserSession.Instance.AlreadyExistsMail( mail);
						//retorna si se pudo logear o no
						if (status)
						{
							msg = "existe";
							indicatorMsgVisibility = true;
						}
						else
						{
							if (!(UserSession.Instance.GetError()==null))
							{
								msg = UserSession.Instance.GetError().GetMessage();
								msg = "no existe";
								indicatorMsgVisibility = true;
							}
							else 
							{
								msg = "no existe";
								indicatorMsgVisibility = true;
							}
							indicatorMsgVisibility = true;
						}
						indicatorLoading = false;

					}, (par) => { return true;});
				}
				return _verifyMailCommand;
			}		
		}
		ICommand _closeMsgDialog = null;
		public ICommand CloseMsgDialgog 
		{
			get 
			{
				if (_closeMsgDialog == null)
					_closeMsgDialog = new Command(() => { indicatorMsgVisibility = false;}, () => { return true;});
				return _closeMsgDialog;
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this,
					new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
