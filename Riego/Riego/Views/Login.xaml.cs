﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Riego
{
	public partial class Login : ContentPage
	{
		LoginViewModel vm = null;
		public Login()
		{
			InitializeComponent();
			vm= new LoginViewModel();
			this.BindingContext = vm;
			//indicatorLoading.BackgroundColor = Color.Red;//Color.FromRgba(0,0,0,1);
		}

		 void  Handle_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
		{
			vm.VerifyMailCommand.Execute(null);
		}
	}
}
