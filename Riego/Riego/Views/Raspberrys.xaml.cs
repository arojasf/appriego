﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Riego
{
	public partial class Raspberrys : ContentPage
	{
		DashboardViewModel dashboardViewModel;

		public Raspberrys(DashboardViewModel dashboardViewModel)
		{
			InitializeComponent();
			this.dashboardViewModel = dashboardViewModel;
			BindingContext = dashboardViewModel;
		}
	}
}
