﻿using System;
using System.Threading.Tasks;

namespace Riego
{
	public interface IListRaspberry
	{
		Task<Tuple<bool, ListRaspberry>> ListRaspberryFromServer();
		Error GetError();
		bool Populate(string json);
	}
}
