﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Riego
{
	public class ListRaspberry: List<Raspberry>,IListRaspberry
	{
		Error error = null;
		static ListRaspberry l;
		public ListRaspberry()
		{
		}
		public static Task<Tuple<bool, ListRaspberry>> Init()
		{
			l = new ListRaspberry();
			return l.ListRaspberryFromServer();
		}

		public Error GetError()
		{
			throw new NotImplementedException();
		}

		string parameterForRaspberry() 
		{
			dynamic obj = new
			{
				mail = UserSession.Instance.mail,
				app = Constants.app
			};
			dynamic usuario = new
			{
				user = obj
			};
			var json = JsonConvert.SerializeObject(usuario);
			return json;
		}

		public async  Task<Tuple<bool,ListRaspberry>> ListRaspberryFromServer()
		{
			bool status = false;
			var res = await ConnectionManager.Post(Constants.endPointRaspberry, parameterForRaspberry());
			if (res.Item1 == null)
			{
				//  res.item2 que treae el contenido
				status = Populate(res.Item2);

			}
			else
			{
				//traigo el error del connectionManager
				error = res.Item1;
			}
			return new Tuple<bool, ListRaspberry>(status, this);
		}

		public bool Populate(string content)
		{
			bool status= false;
			try
			{
				var list = JObject.Parse(content)["raspberrys"];
				var raspberrys = JsonConvert.DeserializeObject<List<Raspberry>>(list.ToString());
				this.AddRange(raspberrys);
				status = true;
			}
			catch (Exception ex)
			{
				error = new Error(EnumErrors.content);
			}

			return status;
		}
	}
}
