﻿using Xamarin.Forms;

namespace Riego
{
	public partial class App : Application
	{
		public static INavigation Navigation = null;
		public App()
		{
			InitializeComponent();
			RestService.Init();
			MainPage = new NavigationPage(new Login());
			Navigation = MainPage.Navigation;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
